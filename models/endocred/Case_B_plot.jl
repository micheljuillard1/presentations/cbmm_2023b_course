using CSV
using Dynare
using DataFrames
using Plots
#using TimeDataFrames
using Dates
#import TimeDataFrames: TimeDataFrame
using Dates, MonthlyDates
using Plots; gr()
theme(:dark)

context = @dynare "Case_B.mod";

#df = Dynare.dataframe(context.results.model_results[1].simulations[1].data)[1:40, :];

endogenous_nbr = context.models[1].endogenous_nbr
df = DataFrame(Dynare.results[:, 1:endogenous_nbr],Dynare.get_endogenous(context.symboltable))[1:20, :];

typeof(df)

dr = QuarterlyDate(2023, 2):Quarter(1):QuarterlyDate(2028, 1)
collect(dr)
df.Data = dr
#@show df


#data = CSV.File("data.csv")
#df = DataFrame(CSV.File("data.csv"))

#dr = [1:1:40;]
#df.Data = dr

p1 = plot(df.Data, df.RS, lw = 3, title = "Fed Funds Rate", xrotation=90, c = :"#fcf803") # Make a line plot
p2 = plot(df.Data, df.PIE4, lw = 3, title = "CORE PCE Y-o-Y Inflation", xrotation=90, c = :"#fcf803")
p3 = plot(df.Data, df.Y, lw = 3, title = "Output Gap", xrotation=90, c = :"#fcf803")
p4 = plot(df.Data, df.TERM10YR, lw = 3, title = "10-Year Term Premium", xrotation=90, c = :"#fcf803")
p5 = plot(df.Data, df.C, lw = 3, title = "CB Credibility Index", xrotation=90, ylims=(0,1), c = :"#fcf803")
p6 = plot(df.Data, df.DL_GDP, lw = 3, title = "Real GDP Growth", xrotation=90, c = :"#fcf803")
p7 = plot(df.Data, df.RS10YR, lw = 3, title = "10-Year Government Bond Yield", xrotation=90, c = :"#fcf803")
p8 = plot(df.Data, df.UNR, lw = 3, title = "Unemployment Rate", xrotation=90, c = :"#fcf803")
p9 = plot(df.Data, df.RR, lw = 3, title = "Real Fed Funds Rate", xrotation=90, c = :"#fcf803")

plt = plot(p1, p2, p3, p4, p5, p6, p7, p8, p9, layout = (3, 3), legend = false)
hline!(plt[1], [3.5], c = :white, linestyle = :dot, lw = 2)
hline!(plt[2], [2], c = :white, linestyle = :dot, lw = 2)
hline!(plt[3], [0], c = :white, linestyle = :dot, lw = 2)
hline!(plt[8], [5], c = :white, linestyle = :dot, lw = 2)
hline!(plt[9], [1.5], c = :white, linestyle = :dot, lw = 2)
for i in 1:9
    vspan!(plt[i], [QuarterlyDate(2023, 2), QuarterlyDate(2028, 1)], label = "", color = :white, alpha = 0.0)
    plot!(xtickfontsize=10,ytickfontsize=10,legendfontsize=10)
    #vspan!(plt[i], label = "", color = :grey, alpha = 0.3)
end

plot!(size=(1500,1500))

savefig("Case_B")
