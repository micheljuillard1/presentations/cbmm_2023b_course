// Simple New Keynesian model, Herbst and Schorfheid (2016)
var y R pi z g YGR INFL INT;
varexo eg ep ez;

parameters tau rho_R psi1 psi2 rho_g rho_z kappa  piA gammaQ rA s_ep s_eg s_ez; 

model;
#  beta = 1/(1 + rA/400);
  y = y(+1) - (1/tau)*(R - pi(+1) - z(+1)) + g - g(+1);
  pi = beta*pi(+1) + kappa*(y - g);
  R = rho_R*R(-1) + (1 - rho_R)*(psi1*pi + psi2*(y - g)) + s_ep*ep/100;
  g = rho_g*g(-1) + s_eg*eg/100;
  z = rho_z*z(-1) + s_ez*ez/100;
  YGR = gammaQ + 100*(y - y(-1) + z);
  INFL = piA + 400*pi;
  INT = piA + rA + 4*gammaQ + 400*R;
end;

steady_state_model;
  y = 0;
  pi = 0;
  R = 0;
  g = 0;
  z = 0;
  YGR = gammaQ;
  INFL = piA;
  INT = piA + rA + 4*gammaQ;
end;

shocks;
  var ep; stderr 1.0;
  var eg; stderr 1.0;
  var ez; stderr 1.0;
  // calibrated measurement errors
  var YGR;  stderr (0.20*0.579923);
  var INFL; stderr (0.20*1.470832);
  var INT;  stderr(0.20*2.237937);
end;

prior(:rA, shape=Gamma,mean=0.5,stdev=0.5);
prior(:piA, shape=Gamma,mean=7,stdev=2);
prior(:gammaQ, shape=Normal, mean=0.4, stdev=0.2);
prior(:tau, shape=Gamma, mean=2, stdev=0.5);
prior(:kappa, shape=Uniform, domain=[0,1]);
prior(:psi1, shape=Gamma, mean=1.5, stdev=0.25);
prior(:psi2, shape=Gamma, mean=0.5, stdev=0.25);
prior(:rho_R,shape=Uniform, domain=[0,1]);
prior(:rho_g, shape=Uniform, domain=[0,1]);
prior(:rho_z, shape=Uniform, domain=[0,1]);
prior(:s_ep, shape=InverseGamma1, mean=0.5013256549262002, variance=0.06867258771281654);
prior(:s_eg, shape=InverseGamma1, mean = 1.2533141373155003, variance=0.4292036732051032);
prior(:s_ez, shape=InverseGamma1, mean = 0.6266570686577502, variance=0.1073009183012758);
plot_priors();


varobs YGR INFL INT;
