# Central Bank Macroeconomic Modeling Workshop April 2023 -- Dynare Estimation

## Schedule

### Monday April 17: Endogenous Monetary Policy Credibility Model (Asya Kostanyan)

### Tuesday April 18: Introduction to Bayesian inference

### Wednesday April 19: Baysesian estimation of DSGE models

### Thursday April 20: Diagnostics

### Friday April 21: Applications

## Videos

* Introduction to Dynare/Julia: [video](https://youtu.be/qVMbVgo-maA)